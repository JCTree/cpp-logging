#include "logging.hh"
#include <cstdarg>
#include <cstdio>
#include <ctime>
#include <string>

// Static member variables
logging::logger logging::m_logger = logging::logger();
bool logging::m_instantiated = false;

// Class logging::logger
logging::logger::logger() {}

logging::logger::~logger() { fclose(log); }

void logging::logger::config(std::string const &filename, level_t _level,
                             std::string const &filemode) {
  level = _level;
  if (filename == "stdout") {
    log = stdout;
  } else if (filename == "stderr") {
    log = stderr;
  } else {
    log = fopen(filename.c_str(), filemode.c_str());
    if (log == NULL) {
      log = stdout;
    }
  }
}

void logging::logger::write(const char *format, va_list args) {
  vfprintf(log, format, args);
  fflush(log);
}

int logging::logger::getLevel() { return level; }

// Class logging
logging::logging(std::string const &logger_name /* = __builtin_FILE()*/) {
  name = "[" + std::regex_replace(logger_name, file_regexp, "");
}

void logging::config(std::string const &filename, level_t _level,
                     std::string const &filemode, std::string const &format,
                     std::string const &time_fmt) {
  if (m_instantiated) {
    return;
  }
  m_instantiated = true;
  m_logger.config(filename, _level, filemode);
}

void logging::updateTime() {
  now = time(nullptr);
  tstruct = *localtime(&now);
  if (strftime(tbuf, sizeof(tbuf), time_fmt.c_str(), &tstruct) == 0) {
    // TODO buffer was too short
  }
}

void logging::_impl_debug(std::string format, uint line, ...) {
  if (0 < m_logger.getLevel()) {
    return;
  }
  std::va_list args;
  va_start(args, line);
  updateTime();
  format =
      tbuf + name + ":" + std::to_string(line) + "] DEBUG: " + format + "\n";
  m_logger.write(format.c_str(), args);
  va_end(args);
}

void logging::_impl_info(std::string format, uint line, ...) {
  if (1 < m_logger.getLevel()) {
    return;
  }
  std::va_list args;
  va_start(args, line);
  updateTime();
  format =
      tbuf + name + ":" + std::to_string(line) + "] INFO: " + format + "\n";
  m_logger.write(format.c_str(), args);
  va_end(args);
}

void logging::_impl_warning(std::string format, uint line, ...) {
  if (2 < m_logger.getLevel()) {
    return;
  }
  std::va_list args;
  va_start(args, line);
  updateTime();
  format =
      tbuf + name + ":" + std::to_string(line) + "] WARNING: " + format + "\n";
  m_logger.write(format.c_str(), args);
  va_end(args);
}

void logging::_impl_error(std::string format, uint line, ...) {
  if (3 < m_logger.getLevel()) {
    return;
  }
  std::va_list args;
  va_start(args, line);
  updateTime();
  format =
      tbuf + name + ":" + std::to_string(line) + "] ERROR: " + format + "\n";
  m_logger.write(format.c_str(), args);
  va_end(args);
}

void logging::_impl_critical(std::string format, uint line, ...) {
  if (4 < m_logger.getLevel()) {
    return;
  }
  std::va_list args;
  va_start(args, line);
  updateTime();
  format =
      tbuf + name + ":" + std::to_string(line) + "] CRITICAL: " + format + "\n";
  m_logger.write(format.c_str(), args);
  va_end(args);
}
