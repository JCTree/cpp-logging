#ifndef LOGGING_HH
#define LOGGING_HH
#include <cstdarg>
#include <iostream>
#include <regex>
#include <string>

class logging {
public:
  enum level_t { DEBUG = 0, INFO = 1, WARNING = 2, ERROR = 3, CRITICAL = 4 };

private:
  // All instances need to share a logger so there is only one fstream open
  class logger {
  private:
    FILE *log = NULL;
    level_t level;

  public:
    logger();
    ~logger();
    void config(std::string const &filename, level_t _level,
                std::string const &filemode);
    void write(const char *format, std::va_list args);
    int getLevel();
  };

  static logger m_logger;
  static bool m_instantiated;
  std::string name;

  time_t now;
  char tbuf[100];
  struct tm tstruct;
  std::string time_fmt = "%H:%M:%S";

  std::regex file_regexp = std::regex("(\\/*[a-zA-Z_.]*)*\\/");

private:
  void updateTime();

public:
  logging(std::string const &name = __builtin_FILE());
  void config(std::string const &filename = "stdout",
              logging::level_t _level = logging::INFO,
              std::string const &filemode = "a", std::string const &format = "",
              std::string const &time_fmt = "%H:%M:%S");

  // These are for nice LSP autocompletions, and are not implemented
  void debug(std::string format, ...);
  void info(std::string format, ...);
  void warning(std::string format, ...);
  void error(std::string format, ...);
  void critical(std::string format, ...);

  void _impl_debug(std::string format, uint line, ...);
  void _impl_info(std::string format, uint line, ...);
  void _impl_warning(std::string format, uint line, ...);
  void _impl_error(std::string format, uint line, ...);
  void _impl_critical(std::string format, uint line, ...);
};

#define debug(format, ...) _impl_debug(format, __builtin_LINE(), __VA_ARGS__)
#define info(format, ...) _impl_info(format, __builtin_LINE(), __VA_ARGS__)
#define warning(format, ...)                                                   \
  _impl_warning(format, __builtin_LINE(), __VA_ARGS__)
#define error(format, ...) _impl_error(format, __builtin_LINE(), __VA_ARGS__)
#define critical(format, ...)                                                  \
  _impl_critical(format, __builtin_LINE(), __VA_ARGS__)

#endif // !LOGGING_HH
